import ast
import io
import os
import sys
import threading
from typing import *

import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasAgg

import cv2
import decord
import PySimpleGUI as sg
import numpy as np
import torch
import vlc
from decord import VideoReader

from facemeshdetector import FaceMeshDetector


PLATFORM = sys.platform
VIDEO_EXTS = ("mp4", "mts", "mov", "avi", "MP4", "MTS", "MOV", "AVI")
FRAMES_PER_BATCH = 500
DEFAULT_ANNO_FOLDER = "./annotations"
FACE_DETECTOR = FaceMeshDetector(maxFaces=2)
sg.theme("DarkBlue")


def get_all_files(path, prefix="", suffix="", contains=""):
    if not os.path.isdir(path):
        raise ValueError(f"{path} is not a valid directory.")
    files = []
    for pre, dirs, basenames in os.walk(path):
        for name in basenames:
            if name.startswith(prefix) and name.endswith(suffix) and contains in name:
                files.append(os.path.join(pre, name))
    return files


def special_set_size(element, size):
    # Only work for sg.Column when `scrollable=True` or `size not (None, None)`
    options = {"width": size[0], "height": size[1]}
    if element.Scrollable or element.Size != (None, None):
        element.Widget.canvas.configure(**options)
    else:
        element.Widget.pack_propagate(0)
        element.set_size(size)


def special_place(elem):
    """
    Places element provided into a Column element so that its placement in the layout is retained.
    :param elem: the element to put into the layout
    :return: A column element containing the provided element
    """
    return sg.Column([[elem]], pad=(0, 0))


def get_video_reader(window, path):
    vr = VideoReader(path, ctx=decord.cpu(0))
    print(f"Num video frames = {len(vr)}")
    window.write_event_value("-VID_READER-", vr)


def load_frames(vr, length, frame_start, frame_end):
    if frame_end >= length:
        frame_end = length - 1
    decord.bridge.set_bridge("torch")
    buf = vr.get_batch(list(range(frame_start, frame_end)))
    return buf, frame_start, frame_end


def display_frame(frame, show_landmarks=False):
    frame_ = torch.clone(frame).numpy()
    plt.close("all")  # erases previously drawn plots
    fig, ax = plt.subplots(1)
    fig.subplots_adjust(left=0, right=1)
    if show_landmarks:
        _, faces = FACE_DETECTOR.findFaceMesh(frame_)
    # This commented code show how to get the mask from the landmarks
    # if len(faces) > 0 :
    #     face = np.array(faces[0], dtype=np.int32)
    #     sel = [10, 338, 297, 332, 284, 251, 389, 356, 454, 323, 361, 288, 397, 365, 379, 378, 400, 377, 
    #             152, 148, 176, 140, 150, 136, 172, 58, 132, 93, 234, 127, 162, 21, 54, 103, 67, 109]
    #     face = face[sel]
    #     img2 = cv2.polylines(frame_, [face], True, (0, 0, 0), thickness=2, lineType=cv2.LINE_8)
    #     frame_ = cv2.fillPoly(img2, [face], (0, 0, 0), lineType=cv2.LINE_AA)
    ax.imshow(frame_, aspect="auto", extent=(0, 1, 1, 0))
    ax.axis("tight")
    ax.axis("off")
    canv = FigureCanvasAgg(fig)
    buf = io.BytesIO()
    canv.print_figure(buf, format="png")
    if buf is not None:
        buf.seek(0)
        return buf
    else:
        return None


def process_annotation(text):
    l = []
    for i in ast.literal_eval(text):
        r = None
        if isinstance(i, list):
            if len(i) == 2:
                r = list(range(*i))
            elif len(i) == 1:
                r = i
            else:
                raise ValueError(
                    f"list i={i} is not allowed (len(i) can only be 1 or 2)"
                )
        elif isinstance(i, int):
            r = [i]
        else:
            raise ValueError(f"{type(i)} is not allowed")
        l = list(set(l) | set(r))
    return l


def video_ctrl_btn(name):
    return sg.Button(name, size=(6, 1), pad=(1, 1))


file_browse_col = [
    [
        sg.Text("Folder"),
        sg.In(size=(50, 10), enable_events=True, key="-FOLDER_LOCATION-"),
        sg.FolderBrowse(),
    ],
    [
        sg.Listbox(
            values=[],
            enable_events=True,
            size=(50, 45),
            auto_size_text=True,
            key="-FILE_LIST-",
        )
    ],
]

video_col = [
    [
        sg.Input(
            default_text="Enter Local Path:", size=(30, 1), key="-VIDEO_LOCATION-"
        ),
        sg.Button("load"),
    ],
    [sg.Image("", size=(640, 480), key="-VID_OUT-")],
    [
        sg.Slider(
            range=(0, 1000),
            default_value=0,
            size=(5, 10),
            orientation="horizontal",
            disable_number_display=True,
            enable_events=True,
            key="-VID_SLIDER-",
        )
    ],
    [video_ctrl_btn(i) for i in ["play", "pause", "stop"]],
    [sg.Text("Load media to start", key="-MESSAGE_AREA-")],
]

frame_col = [
    [sg.Text(size=(40, 1), key="-VID_NAME-", expand_x=True)],
    [sg.Text(size=(40, 1), key="-FRAME_INFO-", expand_x=True)],
    [sg.Text(size=(40, 1), key="-CURR_FRAME-", expand_x=True)],
    [sg.Checkbox(
        text=f"Toggle facial landmarks", enable_events=True, key="-TOGGLE_LANDMARKS-"
    )],
    [sg.Image("", size=(640, 480), key="-FRAME_OUT-")],
    [sg.Button(button_text=f"Load next {FRAMES_PER_BATCH} frames", key="-LOAD_FRAMES-")],
    [
        sg.Button(button_text=f"<", key="-PREV_FRAME-"),
        sg.Slider(
            range=(0, FRAMES_PER_BATCH - 1),
            default_value=0,
            size=(20, 10),
            orientation="horizontal",
            enable_events=True,
            key="-FRAME_SLIDER-",
        ),
        sg.Button(button_text=f">", key="-NEXT_FRAME-"),
    ],
    [
        sg.Text("Annotation Folder"),
        sg.In(
            default_text=DEFAULT_ANNO_FOLDER,
            size=(30, 1),
            enable_events=True,
            key="-ANNO_FOLDER_LOCATION-",
        ),
        sg.FolderBrowse(),
    ],
    [sg.Multiline(size=(15, 10), auto_size_text=True, rstrip=True, key="-ANNOTATION-")],
    [sg.Button(button_text="Submit annotation", key="-SUBMIT_ANNOTATION-")],
]

layout = [
    [
        sg.pin(sg.Column(
                file_browse_col,
                element_justification="c",
                key="-FILE_BROWSE_COL-",
                pad=(0, 0),
        )),
        sg.VSeperator(),
        sg.Column(video_col, element_justification="c", key="-VIDEO_COL-", pad=(0, 0)),
        sg.VSeperator(),
        sg.Column(frame_col, element_justification="c", key="-FRAME_COL-", pad=(0, 0)),
    ]
]

# Initiate the main window
window = sg.Window(
    "Mini Player",
    layout,
    element_justification="center",
    use_default_focus=False,
    finalize=True,
    resizable=True,
)

# Create bindings to specific elements
window["-VIDEO_LOCATION-"].bind("<Button-1>", "+LEFT_CLICK+")
window.bind("<Control-b>", "-FILE_BROWSE_COL-+TOGGLE_STATE+")
window.bind("<Button-3>", "-ADD_TO_ANNO-")

# Set elements to fill empty spaces automatically
[
    window[f"-{i}-"].expand(True, True)
    for i in [
        "VID_OUT",
        "VID_SLIDER",
        "FRAME_SLIDER",
        "VIDEO_COL",
        "FRAME_COL",
        "FILE_LIST",
        "ANNOTATION",
    ]
]

# ------------ Media Player Setup ---------#

inst = vlc.Instance()
list_player = inst.media_list_player_new()
media_list = inst.media_list_new([])
list_player.set_media_list(media_list)
player = list_player.get_media_player()
if PLATFORM.startswith("linux"):
    player.set_xwindow(window["-VID_OUT-"].Widget.winfo_id())
else:
    player.set_hwnd(window["-VID_OUT-"].Widget.winfo_id())

# ------------ Media Player Setup ---------#


# ------------ Program's internal states ------------#

frame_displaying = None
video_reader = None
video_buf = None
slider_val = 0
frame_start = 0
frame_end = FRAMES_PER_BATCH
frame_start_ = 0
frame_end_ = FRAMES_PER_BATCH
video_len = 0
video_path = ""
show_landmarks = False

def reset_all_internal_states():
    global frame_displaying, video_reader, video_buf, slider_val, frame_start, \
        frame_end, frame_start_, frame_end_, video_len, video_path
    frame_displaying = None
    video_reader = None
    video_buf = None
    slider_val = 0
    frame_start = 0
    frame_end = FRAMES_PER_BATCH
    frame_start_ = 0
    frame_end_ = FRAMES_PER_BATCH
    video_len = 0
    video_path = ""

annotation_folder = DEFAULT_ANNO_FOLDER
# try to create the folder
try:
    if not os.path.exists(annotation_folder):
        os.makedirs(annotation_folder)
except Exception as E:
    print(f"** Error {E} **")

# ------------ Program's internal states ------------#


# ------------ The Event Loop ------------#
while True:
    try:
        pass
    except KeyboardInterrupt:
        break

    # run with a timeout so that current location can be updated
    event, values = window.read(timeout=300)
    if event == sg.WIN_CLOSED:
        break

    # print(f"{event}: {values}")

    if event == "-FOLDER_LOCATION-":
        try:
            file_list = get_all_files(values["-FOLDER_LOCATION-"], suffix=VIDEO_EXTS)
        except Exception as E:
            print(f"** Error {E} **")
            file_list = []
        window["-FILE_LIST-"].update(file_list)
    elif event == "-FILE_LIST-":
        try:
            filename = os.path.join(
                values["-FOLDER_LOCATION-"], values["-FILE_LIST-"][0]
            )
            window["-VIDEO_LOCATION-"].update(filename)
        except Exception as E:
            print(f"** Error {E} **")
            pass  # something weird happened making the full filename

    if (
        event == "-VIDEO_LOCATION-+LEFT_CLICK+"
        and values["-VIDEO_LOCATION-"] == "Enter Local Path:"
    ):
        window["-VIDEO_LOCATION-"].update("")

    if event == "-FILE_BROWSE_COL-+TOGGLE_STATE+":
        window["-FILE_BROWSE_COL-"].update(
            visible=not window["-FILE_BROWSE_COL-"]._visible
        )

    if event == "-VID_SLIDER-":
        if player.is_playing():
            player.set_time(int(player.get_length() * (values["-VID_SLIDER-"] / 1000)))

    if event == "play":
        list_player.play()
    if event == "pause":
        list_player.pause()
    if event == "stop":
        list_player.stop()
    if event == "load":
        if player.is_playing():
            list_player.stop()
        if values["-VIDEO_LOCATION-"] != "Enter Local Path:":
            reset_all_internal_states()
            media_list.release()
            media_list = inst.media_list_new([])
            media_list.add_media(values["-VIDEO_LOCATION-"])
            list_player.set_media_list(media_list)
            video_path = values["-VIDEO_LOCATION-"]
            window["-VIDEO_LOCATION-"].update("Enter Local Path:")
            threading.Thread(
                target=get_video_reader,
                args=(
                    window,
                    values["-VIDEO_LOCATION-"],
                ),
                daemon=True,
            ).start()

    if event == "-VID_READER-":
        window["-VID_NAME-"].update(video_path)
        window["-ANNOTATION-"].update(value="")
        video_reader = values["-VID_READER-"]
        video_len = len(video_reader)

    if event == "-LOAD_FRAMES-" and video_reader:
        if frame_start < video_len - 1:
            video_buf, frame_start, frame_end = load_frames(
                video_reader, video_len, frame_start, frame_end
            )
            frame_start_, frame_end_ = frame_start, frame_end
            window["-FRAME_INFO-"].update(
                f"Total: {video_len} | Frames: {frame_start} -> {frame_end} | Resolution = {video_buf.shape[1]}x{video_buf.shape[2]}"
            )
            window["-FRAME_SLIDER-"].update(
                range=(0, frame_end - frame_start - 1), value=0
            )
            if frame_end < video_len - 1:
                frame_start = frame_end
                frame_end += FRAMES_PER_BATCH
        else:
            print("out of frames")

    if event == "-FRAME_SLIDER-" and video_buf is not None:
        slider_val = int(values["-FRAME_SLIDER-"])
        window["-CURR_FRAME-"].update(f"Current frame: {frame_start_ + slider_val}")
        frame_displaying = video_buf[slider_val]
        buf = display_frame(frame_displaying, show_landmarks)
        window["-FRAME_OUT-"].update(data=buf.read())

    if event == "-PREV_FRAME-" and video_buf is not None:
        if int(values["-FRAME_SLIDER-"]) > 0:
            slider_val -= 1
            window["-FRAME_SLIDER-"].update(value=slider_val)
            window["-CURR_FRAME-"].update(f"Current frame: {frame_start_ + slider_val}")
            frame_displaying = video_buf[slider_val]
            buf = display_frame(frame_displaying, show_landmarks)
            window["-FRAME_OUT-"].update(data=buf.read())

    if event == "-NEXT_FRAME-" and video_buf is not None:
        if int(values["-FRAME_SLIDER-"]) < len(video_buf)-1:
            slider_val += 1
            window["-FRAME_SLIDER-"].update(value=slider_val)
            window["-CURR_FRAME-"].update(f"Current frame: {frame_start_ + slider_val}")
            frame_displaying = video_buf[slider_val]
            buf = display_frame(frame_displaying, show_landmarks)
            window["-FRAME_OUT-"].update(data=buf.read())

    if event == "-TOGGLE_LANDMARKS-" and frame_displaying is not None:
        show_landmarks = values["-TOGGLE_LANDMARKS-"]
        buf = display_frame(frame_displaying, show_landmarks)
        window["-FRAME_OUT-"].update(data=buf.read())

    if event == "-ANNO_FOLDER_LOCATION-":
        annotation_folder = values["-ANNO_FOLDER_LOCATION-"]
        # try to create the folder
        try:
            if not os.path.exists(annotation_folder):
                os.makedirs(annotation_folder)
        except Exception as E:
            print(f"** Error {E} **")
            pass

    if (
        event == "-SUBMIT_ANNOTATION-"
        and values["-ANNOTATION-"] != ""
        and annotation_folder != ""
    ):
        try:
            annotation = process_annotation(values["-ANNOTATION-"])
        except Exception as E:
            print(f"** Error {E} **")
            pass
        # now save the annotation
        try:
            folder, basename = os.path.split(os.path.abspath(video_path))
            filename, extension = os.path.splitext(basename)
            with open(f"{annotation_folder}/{filename}.txt", "w") as f:
                for i in annotation:
                    f.write(str(i) + "\n")
        except Exception as E:
            print(f"** Error {E} **")
            pass

    if event == "-ADD_TO_ANNO-":
        curr_anno = values["-ANNOTATION-"]
        window["-ANNOTATION-"].update(value=f"{curr_anno} {frame_start_ + slider_val}")

    # update elapsed time if there is a video loaded and the player is playing
    if player.is_playing():
        window["-VID_SLIDER-"].update(
            value=int(player.get_time() / player.get_length() * 1000)
        )
        window["-MESSAGE_AREA-"].update(
            "{:02d}:{:02d} / {:02d}:{:02d}".format(
                *divmod(player.get_time() // 1000, 60),
                *divmod(player.get_length() // 1000, 60),
            )
        )
    else:
        window["-MESSAGE_AREA-"].update(
            "Load media to start" if media_list.count() == 0 else "Ready to play media"
        )

window.close()
